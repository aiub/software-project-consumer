import React, { Component } from "react";
import styles from "./footer.module.css";
import Grid from "@material-ui/core/Grid";
class Footer extends Component {
  render() {
    return (
      <div className={styles.background}>
        <Grid container spacing={24}>
          <Grid item xs={12} className={styles.head}>
            <h1>Pachpai.com</h1>
            <p classname={styles.textGap}>This is a mave product</p>
            <div className={styles.iconbox}>
              <ul className={styles.icons}>
                <li>
                  <a href="#">
                    <i class="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fab fa-youtube" />
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fab fa-linkedin-in" />
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fab fa-instagram" />
                  </a>
                </li>
              </ul>
            </div>
          </Grid>
        </Grid>

        <Grid
          container
          spacing={24}
          style={{ marginTop: "50px", textAlign: "center" }}
        >
          <Grid item md={4} sm={6} xs={12}>
            <div className={styles.bottomhead}>
              <h3>Pachpai</h3>
              <div className={styles.hr} />
              <p className={styleMedia.linkText}>
                <a href="#">Pachpai Store</a>
              </p>
              <br />
              <p className={styleMedia.linkText}>
                <a href="#">Sell Your Product</a>
              </p>
              <br />
              <p className={styleMedia.linkText}>
                <a href="#">Pachpai Business Policy</a>
              </p>
              <br />
              <p className={styleMedia.linkText}>
                <a href="#">Help</a>
              </p>
              <br />
            </div>
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <div className={styles.bottomhead}>
              <h3>Mave Information</h3>
              <div className={styles.hr} />
              <p className={styleMedia.linkText}>
                <a href="#">Maveteam.com</a>
              </p>
              <br />
              <p className={styleMedia.linkText}>
                <a href="#">Mave Services</a>
              </p>
              <br />
              <div className={styles.iconbox}>
                <ul className={styles.icons}>
                  <li>
                    <a href="#">
                      <i class="fab fa-facebook-f" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fab fa-youtube" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fab fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fab fa-linkedin-in" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fab fa-instagram" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <div className={styles.bottomhead}>
              <h3>Pages</h3>
              <div className={styles.hr} />
              <p className={styleMedia.linkText}>
                <a href="#">Home</a>
              </p>
              <br />
              <p className={styleMedia.linkText}>
                <a href="#">Pachpai Shop</a>
              </p>
              <br />
              <p className={styleMedia.linkText}>
                <a href="#">Blog</a>
              </p>
              <br />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Footer;
