import React, { Component } from "react";
import styles from "./categoryMenu.module.css";

class CategoryMenu extends Component {
  render() {
    return (
      <ul className={styles.list}>
        <li className={styles.catagory}>{this.props.departmentName}</li>
        {this.props.categories.map((item, i) => (
          <li key={i} onClick={() => this.props.filterProduct(item)}>{item}</li>
        ))}
      </ul>
    );
  }
}

export default CategoryMenu;
