import React, { Component } from "react";

class Clock extends Component{
    constructor(props){
        super(props);
        this.state={
            days:0,
            hours:0,
            minutes:0,
            seconds:0
        }

    }
    state={
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
    }
    componentWillMount(){
        this.getTimeUntill(this.props.deadline);    
    }
    componentDidMount(){
        setInterval(() => this.getTimeUntill(this.props.deadline),1000)
    }

    leading0=(num)=>{
        return num<10 ? "0"+num :num
    }

    getTimeUntill=(deadline)=>{
        const time=Date.parse(deadline)-Date.parse(new Date());
        const seconds=Math.floor((time/1000)%60);
        const minutes=Math.floor((time/1000/60)%60);
        const hours=Math.floor(time/(1000*60*60)%24);
        const days=Math.floor(time/(1000*60*60*24));
       this.setState({
           days:days,seconds:seconds,minutes:minutes,hours:hours
       })
    }
    render(){
        
        return(
            <div>
                <div className="clock"> {this.props.discountPercentage}% of till {this.leading0(this.state.seconds)} seconds {this.leading0(this.state.minutes)} minutes {this.leading0(this.state.hours)} hours {this.leading0(this.state.days)} days</div>   
            </div>
        )
    }
}
export default Clock;