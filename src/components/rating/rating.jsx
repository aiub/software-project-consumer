import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import styles from './rating.module.css';

const labels = {
    0.5: 'Useless',
    1: 'Useless+',
    1.5: 'Poor',
    2: 'Poor+',
    2.5: 'Ok',
    3: 'Ok+',
    3.5: 'Good',
    4: 'Good+',
    4.5: 'Excellent',
    5: 'Excellent+',
};






export default function HoverRating(props) {
    const value = props.value;

    const [hover, setHover] = React.useState(-1);
    return (
        <div>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <div className={styles.rating1}>
                    <Rating
                        name="hover-side"
                        value={value}
                        precision={0.5}
                        readOnly
                        onChangeActive={(event, newHover) => {
                            setHover(newHover);
                        }}
                    />
                    <Box ml={value} className={styles.type}>{labels[hover !== -1 ? hover : value]}</Box>
                </div>
            </Box>

        </div>
    );
}
