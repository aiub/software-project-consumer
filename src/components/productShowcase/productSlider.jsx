import React from "react";
import Whirligig from "react-whirligig";
import Grid from "@material-ui/core/Grid";
import styles from "./productShowcase.module.css";
import Product from "./product";


const Slider = props => {
  let whirligig;
  const next = () => whirligig.next();
  const prev = () => whirligig.prev();

  return (
    <div className={styles.sliderBox}>
      <Whirligig
        visibleSlides={4}
        gutter=".3em"
        ref={_whirligigInstance => {
          whirligig = _whirligigInstance;
        }}
        className={styles.slider}
      >
        {props.products.map((product, index) => {
          return (
            <Product key={index} product={product} index={index} />
          );
        })}
      </Whirligig>
      <button onClick={prev} className={styles.prevButton}>
        {"<"}
      </button>
      <button onClick={next} className={styles.nextButton}>
        {">"}
      </button>
    </div>
  );
};

export default Slider;
