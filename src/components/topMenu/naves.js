import Home from "../../views/home/home";
import * as path from "../../routes/slugs";



const naves = [
    {
        path: path.HOME_PATH,
        name: "HOME",
        component: Home
    },
    {
        path: path.PACHPAI_SHOP_PATH,
        name: "PACHPAI SHOP",
        component: null,
    },
    {
        path: "/blog",
        name: "BLOG",
        component: null,
    },
    {
        path: path.SELL_PRODUCT_PATH,
        name: "SELL PRODUCT",
        component: null,
    },
    {
        path: path.CUSTOMER_SERVICE_PATH,
        name: "CUSTOMER SERVICE",
        component: null,
    }

];

export default naves;
