import React, { useState } from "react";

import { Grid } from "@material-ui/core";
import styles from "./imageView.module.css";

export default function Img(props) {
    let [url, setUrl] = useState(props.src);
    let [imgHeight, setImgHeight] = useState(
        props.imgHeight ? props.imgHeight : "200"
    );
    let [imgWidth, setImgWidth] = useState(
        props.imgWidth ? props.imgWidth : "300"
    );
    let [resultHeight, setresultHeight] = useState(500);
    let [resultWidth, setresultWidth] = useState(600);
    let [display, setDisplay] = useState("none");
    let [backgroundPosition, setBackgroundPosition] = useState("0% 0%");
    let [backgroundSize, setBackgroundSize] = useState(
        `${400 * 3}px ${300 * 3}px`
    );

    function lengthToNumber(n) {
        let number = "";
        for (let i = 0; i < n.length; i++) {
            if (n[i] >= 0 && n[i] <= 9) {
                number += n[i];
            }
        }
        return Number(number);
    }
    function mouseMoveFunction(e) {
        e.preventDefault();
        setDisplay("block");
        // imageZoom("imageId", "resultId");
        const { left, top, width, height } = e.target.getBoundingClientRect();
        const x = ((e.pageX - left) / width) * 100;
        const y = ((e.pageY - top) / height) * 100;
        let cx = 300 / 40; //lengthToNumber(resultWidth) / 40;
        let cy = 300 / 40; //lengthToNumber(resultHeight) / 40;

        setBackgroundPosition(`${x}% ${y}%`);
        setBackgroundSize(`${imgWidth * cx}px ${imgHeight * cy}px`);
    }
    function mouseLeaveFunction() {
        setDisplay("none");
    }

    return (
        <div
            onMouseMove={e => mouseMoveFunction(e)}
            onMouseLeave={mouseLeaveFunction}
            style={{
                backgroundPosition: backgroundPosition,
                position: "relative",
                height: imgHeight + "%",
                width: imgWidth + "%",
                border: "1px solid blue",


            }}
        // height={imgHeight}
        // width={imgWidth}
        >
            <img src={props.src} alt="" height="100%" width="100%" />

            <div
                style={{
                    position: "absolute",
                    left: "100%",
                    top: "0%",
                    height: resultHeight + "px",
                    width: resultWidth + "px",
                    display: display,
                    backgroundImage: `url(${props.src})`,
                    backgroundPosition: backgroundPosition,
                    backgroundSize: backgroundSize,
                    backgroundRepeat: "none",
                    zIndex: 1000
                }}
                id="result"
            />
        </div>
    );
}

function imageZoom(imgID, resultID) {
    var img, lens, result, cx, cy;
    img = document.getElementById(imgID);
    result = document.getElementById(resultID);
    /*create lens:*/
    lens = document.createElement("DIV");
    lens.setAttribute("class", styles.imgZoomLens);
    /*insert lens:*/
    img.parentElement.insertBefore(lens, img);
    /*calculate the ratio between result DIV and lens:*/
    cx = result.offsetWidth / lens.offsetWidth;
    cy = result.offsetHeight / lens.offsetHeight;
    /*set background properties for the result DIV:*/
    result.style.backgroundImage = "url('" + img.src + "')";
    result.style.backgroundSize = img.width * cx + "px " + img.height * cy + "px";
    /*execute a function when someone moves the cursor over the image, or the lens:*/
    lens.addEventListener("mousemove", moveLens);
    img.addEventListener("mousemove", moveLens);
    /*and also for touch screens:*/
    lens.addEventListener("touchmove", moveLens);
    img.addEventListener("touchmove", moveLens);
    function moveLens(e) {
        var pos, x, y;
        /*prevent any other actions that may occur when moving over the image:*/
        e.preventDefault();
        /*get the cursor's x and y positions:*/
        pos = getCursorPos(e);
        /*calculate the position of the lens:*/
        x = pos.x - lens.offsetWidth / 2;
        y = pos.y - lens.offsetHeight / 2;
        /*prevent the lens from being positioned outside the image:*/
        if (x > img.width - lens.offsetWidth) {
            x = img.width - lens.offsetWidth;
        }
        if (x < 0) {
            x = 0;
        }
        if (y > img.height - lens.offsetHeight) {
            y = img.height - lens.offsetHeight;
        }
        if (y < 0) {
            y = 0;
        }
        /*set the position of the lens:*/
        lens.style.left = x + "px";
        lens.style.top = y + "px";
        /*display what the lens "sees":*/
        result.style.backgroundPosition = "-" + x * cx + "px -" + y * cy + "px";
    }
    function getCursorPos(e) {
        var a,
            x = 0,
            y = 0;
        e = e || window.event;
        /*get the x and y positions of the image:*/
        a = img.getBoundingClientRect();
        /*calculate the cursor's x and y coordinates, relative to the image:*/
        x = e.pageX - a.left;
        y = e.pageY - a.top;
        /*consider any page scrolling:*/
        x = x - window.pageXOffset;
        y = y - window.pageYOffset;
        return { x: x, y: y };
    }
}
