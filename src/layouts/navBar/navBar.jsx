import React from "react";
import TopBar from "../../components/topBar/topBar";
import TopMenu from "../../components/topMenu/topMenu";

const NavBar = () => (
  <div>
    <TopBar />
    <TopMenu />
  </div>
);

export default NavBar;
