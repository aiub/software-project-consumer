import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
// import tileData from './tileData';
import MultipleSelect from './selection'






const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: '100%',
    height: 650,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
});





const tileData = [
   {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  {
    img: 'https://i.imgur.com/DCdBXcq.jpg',
    title: 'Image',
    author: 'author',
  },
  
 ];


function ProductGallery(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <GridList ellHeight={110} className={classes.gridList} cols={5}>
        {/* <GridListTile  cols={5} style={{ height: 'auto' }}>
          
        


      <MultipleSelect />




        </GridListTile> */}
        {tileData.map(tile => (
          <GridListTile key={tile.img}>
            <img src={tile.img} alt={tile.title} />
            <GridListTileBar
              title={tile.title}
              subtitle={<span>by: {tile.author}</span>}
              actionIcon={
                <IconButton className={classes.icon}>
                  <InfoIcon />
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}

ProductGallery.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductGallery);
