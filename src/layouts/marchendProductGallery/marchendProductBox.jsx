import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ProductGallery from "./productGallery";
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';
import MultipleSelect from './selection'
import SearchButton from './searchButton'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

function MarchendProductBox(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
      <Grid item xs={2}>
          
    </Grid>
        <Grid item xs={12}>
            <Fab variant="extended" aria-label="Delete" className={classes.fab}>
                <NavigationIcon className={classes.extendedIcon} />
            ADD PRODUCT
            </Fab>
        </Grid>

      

        <Grid item xs={12}>
            <h3 className={styleMedia.heading}>Your Products</h3>
            <hr className={styleMedia.hr}/>
            <Grid container spacing={12}>
            <Grid item xs={3}>
            <MultipleSelect />
            </Grid>
            <Grid item xs={9}>
            <SearchButton />
            </Grid>
            </Grid>
        </Grid>

      

        <Grid item xs={12}>
        <ProductGallery />
        </Grid>
        
      </Grid>
    </div>
  );
}

MarchendProductBox.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MarchendProductBox);
