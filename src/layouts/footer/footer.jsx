import React , {Component} from "react";
import FeatureBox from "../../components/bottomArea/bottomArea"
import FooterArea from "../../components/Footer/footer"
import styles from "./footer.module.css";

export default class Footer extends Component{
    render(){
        return(
            <div className={styles.footerArea}>
                <FeatureBox/>
                <FooterArea/>
            </div>
        )
    }
}