import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import HotProduct from "./hotProduct";

import styles from "./hotProducts.module.css";

const dame = theme => ({
  root: {
    flexGrow: 1,
    background: "whitesmoke",
    padding: "20px",
    boxShadow: "0px 0px 30px -12px #e7140a",
    marginBottom: "100px"
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.secondary
  }
});

function HotProducts(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={12} className={styles.head}>
          <h2>HOT PRODUCTS</h2>
          <hr />
        </Grid>
        <Grid item xs={12} md={4} sm={6}>
          <HotProduct />
        </Grid>
        <Grid item xs={12} md={4} sm={6}>
          <HotProduct />
        </Grid>
        <Grid item xs={12} md={4} sm={6}>
          <HotProduct />
        </Grid>

        <Grid item xs={12} md={4} sm={6}>
          <HotProduct />
        </Grid>
        <Grid item xs={12} md={4} sm={6}>
          <HotProduct />
        </Grid>
        <Grid item xs={12} md={4} sm={6}>
          <HotProduct />
        </Grid>
      </Grid>
    </div>
  );
}

HotProducts.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dame)(HotProducts);
