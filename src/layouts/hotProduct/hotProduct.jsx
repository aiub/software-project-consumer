import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import IconButton from "@material-ui/core/IconButton";
import styles from "./hotProducts.module.css";

const iconStyle = {
  color: "#FB8C00",
  paddingTop: "5px",
  fontSize: "20px"
};

function HotProduct(props) {
  return (
    <div className={styles.hotProduct}>
      {/* <Paper className={classes.paper}> */}
      <Grid container spacing={16}>
        <Grid item xs={6}>
          <ButtonBase>
            <img
              className={styles.image}
              alt="complex"
              src="https://cms.qz.com/wp-content/uploads/2016/10/google-goog-pixel-review.jpg?quality=75&strip=all&w=900&h=900&crop=1"
            />
          </ButtonBase>
        </Grid>
        <Grid item xs={6} sm container>
          <Grid item xs container direction="column" spacing={9}>
            <Grid item xs>
              <h4>SMARTPHONE</h4>

              <ul className={styles.list}>
                <li>
                  <span>
                    <i className="material-icons" style={iconStyle}>
                      keyboard_arrow_right
                    </i>
                    <a href="#">Google phone</a>
                  </span>
                </li>
                <li>
                  <span>
                    <i className="material-icons" style={iconStyle}>
                      keyboard_arrow_right
                    </i>
                    <a href="#">Shaomi</a>
                  </span>
                </li>
                <li>
                  <span>
                    <i className="material-icons" style={iconStyle}>
                      keyboard_arrow_right
                    </i>
                    <a href="#">Samsung</a>
                  </span>
                </li>
                <li>
                  <span>
                    <i className="material-icons" style={iconStyle}>
                      keyboard_arrow_right
                    </i>
                    <a href="#">Nokia</a>
                  </span>
                </li>
                <li>
                  <span>
                    <i className="material-icons" style={iconStyle}>
                      keyboard_arrow_right
                    </i>
                    <a href="#">LG</a>
                  </span>
                </li>
              </ul>
            </Grid>
            <Grid item>
              {/* <IconButton aria-label="Delete" className={classes.margin}>
                        <DeleteIcon fontSize="small" />
                        </IconButton>
                        <IconButton aria-label="Delete" className={classes.margin}>
                        <DeleteIcon />
                        </IconButton>
                        <IconButton aria-label="Delete" className={classes.margin}>
                        <DeleteIcon fontSize="large" />
                        </IconButton>
 */}
            </Grid>
          </Grid>
          {/* <Grid item>
              <Typography variant="subtitle1">$19.00</Typography>
            </Grid> */}
        </Grid>
      </Grid>
      {/* </Paper> */}
    </div>
  );
}

HotProduct.propTypes = {
  classes: PropTypes.object.isRequired
};

export default HotProduct;
