import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import styles from "./productShowcase.module.css";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import { NavLink } from "react-router-dom";

const content = [
  {
    title: "Vulputate Mollis Ultricies Fermentum Parturient",
    description:
      "Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis.",
    button: "Read More",
    image: "https://i.imgur.com/ZXBtVw7.jpg",
    user: "Luan Gjokaj",
    userProfile: "https://i.imgur.com/JSW6mEk.png"
  },
  {
    title: "Tortor Dapibus Commodo Aenean Quam",
    description:
      "Nullam id dolor id nibh ultricies vehicula ut id elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui.",
    button: "Discover",
    image: "https://i.imgur.com/DCdBXcq.jpg",
    user: "Erich Behrens",
    userProfile: "https://i.imgur.com/0Clfnu7.png"
  },
  {
    title: "Phasellus volutpat metus",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.",
    button: "Buy now",
    image: "https://i.imgur.com/DvmN8Hx.jpg",
    user: "Bruno Vizovskyy",
    userProfile: "https://i.imgur.com/4KeKvtH.png"
  },
  {
    title: "Phasellus volutpat metus",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.",
    button: "Buy now",
    image: "https://i.imgur.com/DvmN8Hx.jpg",
    user: "Bruno Vizovskyy",
    userProfile: "https://i.imgur.com/4KeKvtH.png"
  }
];

class Hot extends Component {
  state = {
    width: 0,
    height: 0
  };

  updateWindowDimensions = this.updateWindowDimensions.bind(this);

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
    console.log(this.state.width);
  }

  render() {
    return (
      <Grid container>
        {content.map((item, index) => {
          const linkTo = {
            pathname: "/product/" + index,
            product: item
          };
          return (
            <Grid item md={3} sm={3} xs={3} key={index}>
              <NavLink to={linkTo}>
                <Card className={styles.productView}>
                  <CardMedia style={{ height: "200px" }} image={item.image} />

                  <CardContent className={styles.productContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Lizard
                    </Typography>
                    <Typography component="p">Lizards are</Typography>
                  </CardContent>

                  <CardActions className={styles.addToCart}>
                    <ul>
                      <Tooltip
                        title={
                          <Typography color="inherit">add to cart</Typography>
                        }
                        placement="top-start"
                      >
                        <li>
                          <i class="material-icons">shopping_cart</i>
                        </li>
                      </Tooltip>
                      <Tooltip
                        title={
                          <Typography color="inherit">
                            add to favorite
                          </Typography>
                        }
                        placement="top-start"
                      >
                        <li>
                          <i class="material-icons">favorite_border</i>
                        </li>
                      </Tooltip>
                      <Tooltip
                        title={
                          <Typography color="inherit">view product</Typography>
                        }
                        placement="top-start"
                      >
                        <li>
                          <i class="material-icons">zoom_in</i>
                        </li>
                      </Tooltip>
                    </ul>
                  </CardActions>
                </Card>
              </NavLink>
            </Grid>
          );
        })}
      </Grid>
    );
  }
}

export default Hot;
