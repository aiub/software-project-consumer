const departments = [
  {
    path: "/fasion",
    name: "FASION",
    component: null,
    layout: "/product"
  },
  {
    path: "/wemen",
    name: "WOMEN",
    component: null,
    layout: "/product"
  },

  {
    path: "/men",
    name: "MEN",
    component: null,
    layout: "/product"
  },
  {
    path: "/accessories",
    name: "ACCESSORIES",
    component: null,
    layout: "/product"
  },

  {
    path: "/sports",
    name: "SPORTS",
    component: null,
    layout: "/product"
  },
  {
    path: "/daily-shop",
    name: "DAILY SHOP",
    component: null,
    layout: "/product"
  },
  {
    path: "/electronics",
    name: "ELECTRONIC",
    component: null,
    layout: "/product"
  },
  {
    path: "/health",
    name: "HEALTH",
    component: null,
    layout: "/product"
  },
  {
    path: "/toy",
    name: "TOY, KIDS and BABY",
    component: null,
    layout: "/product"
  }
];

export default departments;
