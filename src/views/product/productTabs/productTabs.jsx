import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Grid from "@material-ui/core/Grid";
import styles from "./productTabs.module.css";
import { lighten, makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Rate from "../../../components/rating/rating";
import Icon from '@material-ui/core/Icon';
import LinearProgress from "@material-ui/core/LinearProgress";
import Avatar from '@material-ui/core/Avatar';
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

import { getProductAdditionalInfoAction } from "../../../store/products/actions";

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

const BorderLinearProgress = withStyles({
    root: {
        height: 23,
        margin: '0px 0px 10px 0px',
        backgroundColor: '#f5f5f5'
        // backgroundColor: lighten("#f5f5f5", 0.5)
    },
    bar: {
        borderRadius: 20,
        backgroundColor: "#57bb8a"
    }
})(LinearProgress);

export default function TabsWrappedLabel(props) {
    //   const classes = useStyles();
    const [value, setValue] = React.useState('description');

    function handleChange(event, newValue) {
        setValue(newValue);
    }
    const reviewSummery = {
        totalRating: 4200,
        totalReview: 1000,
        ratings: {
            "5": 80,
            "4": 70,
            "3": 50,
            "2": 40,
            "1": 10
        }

    }
    console.log("props.productAdditionalInfo", props.productAdditionalInfo)
    return (
        <div className={styles.productDetailsTab}>
            <Grid container spacing={24}>
                <Grid item md={12} sm={12} xs={12}>
                    <AppBar position="static" className={styles.tabStyle}>
                        <Tabs value={value} onChange={handleChange}>
                            <Tab value="description" label="DESCRIPTION" wrapped />
                            <Tab value="additionalInfo" label="ADDITIONAL INFORMATION" />
                            <Tab value="reviews" label="REVIEWS" />
                            <Tab value="shippingAndDelivery" label="SHIPPING AND DELIVERY" />

                        </Tabs>
                    </AppBar>
                    {
                        value === 'description' &&
                        <TabContainer> {props.productDescription && getProductDescription(props.productDescription)}</TabContainer>
                    }
                    {value === 'additionalInfo' && <TabContainer>{props.productAdditionalInfo && getProductAdditionalInfo(props.productAdditionalInfo)}</TabContainer>}
                    {value === 'reviews' && <TabContainer>{props.productReviews && getProductReview(props.productReviews, reviewSummery)}</TabContainer>}
                    {value === 'shippingAndDelivery' && <TabContainer>{props.shippingInfo && getShippingInfo(props.shippingInfo)}</TabContainer>}

                </Grid>
            </Grid>

        </div>
    );
}

{/*------PRODUCT DESCRIPTION FUNCTION------*/ }
const getProductDescription = (productDescription) => {
    return (
        <Grid container>
            <Grid item xs={12} sm={12} md={12}>
                <h4 className={styles.title}>product Description</h4>
                <p>{productDescription.description}</p>
            </Grid>
        </Grid>
    )
}

{/*------PRODUCTADDITIONAL INFORMATION FUNCTION------*/ }

const getProductAdditionalInfo = (productAdditionalInfo) => {
    return (
        <Grid container>
            {productAdditionalInfo.variants.map((e, i) => {
                if (e.isAvailable) {
                    return (
                        <Table>
                            <TableHead>
                                <TableRow>
                                    {e.attrs.colorFamily ? <TableCell>  COLOR</TableCell> : null}
                                    {e.attrs.size ? <TableCell>  SIZE</TableCell> : null}
                                    {e.quantity ? <TableCell> AVAILABLE QUANTITY</TableCell> : null}
                                    {e.price ? <TableCell> Price</TableCell> : null}
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                <TableRow>
                                    <TableCell>{e.attrs.colorFamily && e.attrs.colorFamily}</TableCell>
                                    <TableCell>{e.attrs.size && e.attrs.size}</TableCell>
                                    <TableCell>{e.quantity && e.quantity}</TableCell>
                                    <TableCell>{e.price && e.price}</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    )
                }

            })}
        </Grid>
    )
}

{/*------PRODUCT SHIPPING FUNCTION------*/ }
const getShippingInfo = (shippingInfo) => {
    return (
        <Grid container>

            {/*------title------*/}
            <Grid item xs={12} sm={12} md={12} >
                <h4 className={styles.title} >SHIPPING INFORMATION</h4>
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={styles.shippingInfo}>
                <table>
                    <tr>
                        <th>Height</th>
                        <th>width</th>
                        <th>length</th>
                        <th>weight</th>
                    </tr>
                    <tr>
                        <td>{shippingInfo.height}</td>
                        <td>{shippingInfo.width}</td>
                        <td>{shippingInfo.length}</td>
                        <td>{shippingInfo.weight}</td>

                    </tr>
                </table>
            </Grid>
        </Grid>
    )
}



{/*------PRODUCT REVIEW FUNCTION------*/ }
const getProductReview = (productReviews, reviewSummery) => {
    return (
        <Grid container>
            {/*------title------*/}
            <Grid item xs={12} sm={12} md={12}>
                <h4 className={styles.title}>REVIEWS</h4>
            </Grid>
            {/*------Review Summery------*/}
            <Grid item xs={12} sm={12} md={12} >
                <Grid container spacing={4}>
                    <Grid item xs={4} sm={4} md={4}>
                        <h1 className={styles.header}>{reviewSummery.totalRating / reviewSummery.totalReview}</h1>
                        <Rate value={4} />
                        <Icon className={styles.accIcon}>people</Icon>
                        <p className={styles.reviewSummery}>{reviewSummery.totalReview} total</p>
                    </Grid>
                    {/*------Rating bar------*/}
                    <Grid item xs={8} sm={8} md={8}>
                        <Grid container>
                            <Grid item xs={12} sm={12} md={12}>
                                <p className={styles.rate}>5</p> <BorderLinearProgress
                                    variant="determinate"
                                    value={reviewSummery.ratings['5']}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={12}>
                                <p className={styles.rate}>4</p> <BorderLinearProgress
                                    variant="determinate"
                                    value={reviewSummery.ratings['4']}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={12}>
                                <p className={styles.rate}>3</p> <BorderLinearProgress
                                    variant="determinate"
                                    value={reviewSummery.ratings['3']}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={12}>
                                <p className={styles.rate}>2</p> <BorderLinearProgress
                                    variant="determinate"
                                    value={reviewSummery.ratings['2']}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={12}>
                                <p className={styles.rate}>1</p> <BorderLinearProgress
                                    variant="determinate"
                                    value={reviewSummery.ratings['1']}
                                />
                            </Grid>
                        </Grid>
                    </Grid>

                    {/*------Comments------*/}
                    <Grid item xs={12} sm={12} md={12}>
                        {productReviews.reviews.map((review, i) => {
                            let reviewDate = new Date(review.comment.updatedAt)
                            let date = new Date(reviewDate.getMonth() + "/" + reviewDate.getDate() + "/" + reviewDate.getUTCFullYear()).toDateString()

                            return (
                                <Grid container spacing={4}>
                                    <Grid item xs={2} sm={1} md={1}>
                                        <Avatar className={styles.avatar}>H</Avatar>
                                    </Grid>

                                    <Grid item xs={10} sm={11} md={11}>
                                        <Grid container>
                                            <Grid item xs={12} sm={12} md={12}>
                                                <Grid container justify="space-around" alignItems="flex-start">
                                                    <Grid item xs={6} sm={6} md={6}>
                                                        <h4>{review && review.customer.name}</h4>
                                                        <Rate value={review && review.rating} />
                                                        <h4> {review && date}</h4>
                                                    </Grid>
                                                    <Grid item xs={6} sm={6} md={6}>

                                                    </Grid>
                                                </Grid>
                                            </Grid>

                                            <Grid item xs={12} sm={12} md={12}>
                                                {review && review.comment.text}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            )
                        }
                        )}

                    </Grid>
                </Grid>
            </Grid>
        </Grid>)
}
















