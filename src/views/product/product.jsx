import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Grid from "@material-ui/core/Grid";
import styles from "./product.module.css";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from '@material-ui/core/Zoom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import { getProductAdditionalInfoAction, getProductDescriptionAction, getProductReviewAction, getProductShippingInfoAction } from "../../store/products/actions";
import { postCartProductsAction } from "../../store/cart/actions";
import ProductTabs from "./productTabs/productTabs";
import Button from '@material-ui/core/Button';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ImageViewer from '../../components/imageView/imageView'
import { ROOT_URL } from "../../helpers/apis";


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(-0.5),
    padding: theme.spacing(1, 0),

  },
  link: {
    display: 'flex',
  },
  icon: {
    marginRight: theme.spacing(0.5),
    width: 20,
    height: 20,
  },
  button: {
    margin: theme.spacing(1),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),

  },
}));

export default function (props) {
  const productInfo = props.location.product;
  const productAdditionalInfo = useSelector(state => state.productReducer.productAdditionalInfo);
  const productDescription = useSelector(state => state.productReducer.productDescription);
  const productReviews = useSelector(state => state.productReducer.productReviews);
  const shippingInfo = useSelector(state => state.productReducer.shippingInfo);
  const cart = useSelector(state => state.cartReducer.cartItems);
  const dispatch = useDispatch();
  const [imgIndex, setImgIndex] = useState(0);
  const [variantIndex, setVariantIndex] = useState(0);
  const classes = useStyles();



  async function getProductInfo() {
    productInfo && dispatch(await getProductAdditionalInfoAction(productInfo._id))
    productInfo && dispatch(await getProductDescriptionAction(productInfo._id))
    productInfo && dispatch(await getProductReviewAction(productInfo._id))
    productInfo && dispatch(await getProductShippingInfoAction(productInfo._id))
  }

  useEffect(() => {
    getProductInfo();

    return function () {
      console.log("unmounted in Product");
    }

  }, []);

  const changeImgIndex = (index) => {
    setVariantIndex(index)
  }

  const onSelectColor = (color) => {
    productAdditionalInfo.variants.forEach((e, i) => {
      if (e.attrs.colorFamily == color) {
        setImgIndex(i)
      }
    }
    )
  }



  const onAddToCart = async (variantIndex) => {
    let slectedProduct = productAdditionalInfo.variants.filter((e, i) => i == variantIndex);


    let cartItem = {
      productId: productInfo._id,
      variantId: slectedProduct[0]._id,
      quantity: 1
    }

    const cart = {
      quantity: 1,
      variant: slectedProduct[0],
      product: productInfo._id
    }

    dispatch(await postCartProductsAction(cartItem, cart));

  }

  return (
    <Grid container style={{ marginBottom: "100px" }} spacing={2}>
      {/*left grid for product image and product varient image*/}
      <Grid item xs={12} sm={12} md={5}>
        {productInfo && productAdditionalInfo && getProductImages(productInfo, productAdditionalInfo, imgIndex, changeImgIndex, variantIndex)}
      </Grid>

      {/*Middle grid for product Details*/}
      <Grid item xs={12} sm={12} md={5}>
        {productInfo && productAdditionalInfo && getProductDetailsInfo(productInfo, productAdditionalInfo, imgIndex, classes, onSelectColor, variantIndex, onAddToCart)}
      </Grid>

      {/*right grid for Delivery info*/}
      <Grid item xs={12} sm={12} md={2}>
        {productInfo && getDeliveryInfo()}
      </Grid>

      <Grid style={{ marginBottom: "100px" }} item xs={12} sm={12} md={12}>
        {productInfo && productAdditionalInfo && <ProductTabs productDescription={productDescription} productReviews={productReviews} shippingInfo={shippingInfo} productAdditionalInfo={productAdditionalInfo} />}
      </Grid>

    </Grid>
  )
}


const getProductImages = (productInfo, productAdditionalInfo, imgIndex, changeImgIndex, variantIndex) => (
  <Grid container spacing={2} >
    {/*variant image section*/}
    <Grid item xs={12} sm={12} md={3} container direction="column" justify="space-evenly" alignItems="flex-start" style={{ overflow: "auto", height: "500px", width: "150px", display: "block" }}>
      {productAdditionalInfo.variants[imgIndex].images.map((e, i) => <img key={i} onClick={() => changeImgIndex(i)} src={ROOT_URL + e} className={styles.thumbnailImage} />)}

    </Grid>
    {/*Product image section*/}
    <Grid item xs={12} sm={12} md={9} container direction="column" justify="space-evenly" alignItems="flex-start" className={styles.productImageBox}>
      <ImageViewer src={ROOT_URL + productAdditionalInfo.variants[imgIndex].images[variantIndex]} imgHeight="100"
        imgWidth="100" />
    </Grid>
  </Grid>
)
const getDeliveryInfo = () => (
  <Grid container className={styles.deliveryInfo}>
    <h4 className={styles.header}>Delivery Information</h4>
    <p>Inside Dhaka :50</p>
    <p>Outside Dhaka :50</p>

  </Grid>

)

const getProductDetailsInfo = (productInfo, productAdditionalInfo, imgIndex, classes, onSelectColor, variantIndex, onAddToCart) => (
  <Grid container>
    <Grid item xs={12} sm={12} md={8}>
      <h1 className={styles.title}>{productInfo.name}</h1>
    </Grid>
    <Grid item xs={12} sm={12} md={4}>
      <a href="#" className={styles.productBrand}><p>{productInfo.brand}</p></a>
    </Grid>
    <Grid item xs={12} sm={12} md={12}>
      <p className={styles.price}>
        <span className={styles.symbol}>BTD</span>{productAdditionalInfo.variants[imgIndex].price}
      </p>
    </Grid>
    <Grid item xs={12} sm={12} md={12}>
      <p className={styles.shortDetails}>{productInfo.shortDetails}</p>
    </Grid>
    <Grid item xs={12} sm={12} md={12}>
      <div className={styles.productConfiguration}>
        <h1> Colors :</h1>
        <ul className={styles.allColors}>
          {productAdditionalInfo.variants.map((e, i) => {
            return (
              <Tooltip key={i} title={e.attrs.colorFamily} placement="top" TransitionComponent={Zoom}>
                <li style={{ background: e.attrs.colorFamily }} onClick={() => onSelectColor(e.attrs.colorFamily)}></li>
              </Tooltip>
            )
          })}
        </ul>
      </div>

    </Grid>
    <Grid item xs={12} sm={12} md={12}  >
      <Grid container direction="row" spacing={5}>
        <Grid item xs={5} sm={5} md={5}>
          <Grid container spacing={0}>
            <Grid item xs={2} sm={2} md={3}>
              <button className={styles.buttonStyle}>
                -
             </button>
            </Grid>
            <Grid item xs={8} sm={8} md={6}>
              <input
                type="text"
                value={1}
                className={styles.quantityBox}

              />
            </Grid>
            <Grid item xs={2} sm={2} md={3}>
              <button className={styles.buttonStyle} >
                +
             </button>
            </Grid>

          </Grid>

        </Grid>

        <Grid item xs={5} sm={5} md={5}>

          <button className={styles.addToCart} onClick={() => onAddToCart(variantIndex)}  >
            ADD TO CART
             </button>
        </Grid>
        <Grid item xs={2} sm={2} md={12}>
          <Button className={classes.button}>
            <FavoriteIcon />Add to wishlist
               </Button>
        </Grid>
      </Grid>



    </Grid>
    <Grid item xs={12} sm={12} md={12}>

    </Grid>
    <Grid item xs={12} sm={12} md={12}>
      <hr />
      <p className={classes.root} style={{ fontWeight: "600", fontSize: "100%", color: "rgb(77, 78, 80)", letterSpacing: "1px", float: "left" }}>Category : </p>
      <Paper elevation={0} >
        <Breadcrumbs aria-label="breadcrumb">
          {productInfo.category.map((e, i) => {
            return (
              <Link
                style={{ marginTop: "2px" }}
                key={i}
                color="inherit"
                href="#"
                className={classes.link}
              >
                {e}
              </Link>
            )
          })}
        </Breadcrumbs>
      </Paper>
    </Grid>
  </Grid >
)


