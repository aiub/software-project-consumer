import React, { useState, useEffect, Fragment } from "react";
import styles from "./order.module.css";
import { Grid } from "@material-ui/core";
import { Delete } from "@material-ui/icons";
import DeleteOutlineRoundedIcon from "@material-ui/icons/DeleteOutlineRounded";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { ROOT_URL } from "../../../../helpers/apis";

import Image from "../../ui/image/image";

export function Order(props) {
    const product = props.product;
    const productQuantity = props.product.cartQuantity;

    return (
        // <React.Fragment>
        <TableRow
        // container
        // className={[styles.order, styles.text].join(" ")}
        // spacing={4}
        >
            {/* Product */}
            <TableCell>
                <a href={product.link ? product.link : null} target="_blank">
                    <Grid container style={{ alignItems: "center" }}>
                        <Grid
                            item
                            xs={props.gridSize.product.img.xs}
                            sm={props.gridSize.product.img.sm}
                            md={props.gridSize.product.img.md}
                            lg={props.gridSize.product.img.lg}
                        >
                            <Image src={ROOT_URL + product.images[0]} />
                        </Grid>
                        <Grid
                            item
                            xs={props.gridSize.product.name.xs}
                            sm={props.gridSize.product.name.sm}
                            md={props.gridSize.product.name.md}
                            lg={props.gridSize.product.name.lg}
                        >
                            <Grid container>
                                <Grid item xs={12}>
                                    Price : {product.price}
                                </Grid>
                                {product.discountAmount ? (
                                    <Grid item xs={12}>
                                        Discount Amount : {product.discountAmount}
                                    </Grid>
                                ) : null}
                                {product.discountPercentage ? (
                                    <Grid item xs={12}>
                                        Discount Percentage : {product.discountPercentage}
                                    </Grid>
                                ) : null}
                                {product.discountAmount || product.discountPercentage ? (
                                    <Grid item xs={12}>
                                        Price after discount: {props.productPrice}
                                    </Grid>
                                ) : null}
                            </Grid>
                        </Grid>
                    </Grid>
                </a>
            </TableCell>

            {/* Quantity */}
            <TableCell>
                <Grid container spacing={0}>
                    <Grid item onClick={props.decreaseQuantity} xs={3}>
                        <button className={styles.quantity}>-</button>
                    </Grid>
                    <Grid item xs={4}>
                        <input
                            type="number"
                            onBlur={e => {
                                if (e.target.value < 1)
                                    props.productQuantity(e, true)
                            }}
                            value={product.cartQuantity}
                            onChange={props.productQuantity}
                            className={styles.quantityBox}
                        />
                    </Grid>
                    <Grid item onClick={props.increaseQuantity} xs={3}>
                        <button className={styles.quantity}>+</button>
                    </Grid>
                </Grid>
            </TableCell>

            {/* Total Pricing */}
            <TableCell>
                <Grid container>
                    {/* <Grid item xs={12}>
            Actual Price: {product.price * productQuantity}
          </Grid>
          <Grid item xs={12}>
            Total Discount: {props.totalDiscount}
          </Grid> */}
                    <Grid item xs={12}>
                        {props.totalPrice}
                    </Grid>
                </Grid>
            </TableCell>

            {/* Actions */}
            <TableCell>
                <button className={styles.delete} onClick={props.deleteItem}>
                    <DeleteOutlineRoundedIcon />
                </button>
            </TableCell>
        </TableRow>
        // {/* </React.Fragment> */ }
    );
}
