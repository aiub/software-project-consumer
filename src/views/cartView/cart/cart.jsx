import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Order } from "./order/order";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

import { Grid } from "@material-ui/core";

import { CartSummary } from "./cart-summary/cart-summary";

export default function Cart(props) {
    const [items, setItems] = useState([]);
    const [deletedItems, setDeletedItems] = useState([]);
    const [undoCount, setUndoCount] = useState(0);
    const [size, setSize] = useState(props.size ? props.size : sizeObject);
    let products = useSelector(state => state.cartReducer.cartItems);

    //storing item in item state
    useEffect(() => {
        products = products.map(product => {
            return product.packagePrice
                ? {
                    ...product,
                    packagePrice: product.packagePrice.sort(
                        (a, b) => b.piece - a.piece
                    ),
                    cartQuantity: 1
                }
                : {
                    ...product,
                    cartQuantity: 1
                };
        });

        setItems(products);
    }, []);

    const productIncreaseHandler = index => {
        let products = [...items];
        let quantity = products[index].cartQuantity + 1;
        if (quantity <= products[index].quantity) {
            products[index] = { ...products[index], cartQuantity: quantity };
            setItems(products);
        } else {
            window.alert(
                "Out of stock. Available quantity is:" +
                products[index].quantity +
                "\n" +
                "Contact with us for purchase more."
            );
        }
    };

    const productDecreaseHandler = index => {
        let products = [...items];
        let quantity = products[index].cartQuantity - 1;
        if (quantity >= 1) {
            products[index] = { ...products[index], cartQuantity: quantity };
            setItems(products);
        } else {
            window.alert("Quantity must be at least one.");
        }
    };

    const setProductQuantityHandler = (e, inValid, index) => {
        let products = [...items];
        let value = e.target.value;
        // console.log(e.target);
        if (inValid) {
            products[index] = {
                ...products[index],
                cartQuantity: 1
            };
        }
        else if (value < 0) {
            products[index] = {
                ...products[index],
                cartQuantity: 1
            };

            window.alert("Quantity must be at least one.");
        } else {
            if (products[index].quantity < value) {
                window.alert(
                    "Out of stock. Available quantity is:" +
                    products[index].quantity +
                    "\n" +
                    "Contact with us for purchase more."
                );
            } else
                products[index] = {
                    ...products[index],
                    cartQuantity: value
                };

        }
        setItems(products);


    };

    const setDeletedItemsHandler = id => {
        let objects = [...items];
        objects = objects.filter(e => {
            if (e._id) {
                if (e._id == id) {
                    setDeletedItems([...deletedItems, e]);
                    // console.log(deletedItems);
                    setUndoCount(undoCount + 1);
                } else {
                    return true;
                }
            }
        });
        setItems(objects);
    };

    const undoDeletedItems = () => {
        if (undoCount === 0) return;
        const itemsArray = [...items, deletedItems[undoCount - 1]];
        // console.log(undoCount,deletedItems);
        setItems(itemsArray);
        setUndoCount(undoCount - 1);
        setDeletedItems(deletedItems.slice(0, deletedItems.length - 1));
        // console.log("undo==>", deletedItems)
    };

    const totalCostWithoutDiscountGenerator = () => {
        let actualCost = 0;
        items.forEach(item => {
            actualCost += Number(item.price * item.cartQuantity);
        });
        return Number(actualCost.toFixed(2));
    };

    const totalCostWithDiscountGenerator = () => {
        let totalCost =
            totalCostWithoutDiscountGenerator() - totalDiscountGenerator();
        return totalCost.toFixed(2);
    };

    const totalDiscountGenerator = () => {
        let price = 0;
        for (let i = 0; i < items.length; i++) {
            // console.log(price)
            price += getTotalPriceOfSingleProduct(i);
        }
        // console.log(price, totalCostWithoutDiscountGenerator());
        return Number(totalCostWithoutDiscountGenerator() - price).toFixed(2);
    };

    const getProductPrice = index => {
        let product = items[index];
        let price = product.price;
        price -= product.discountAmount ? product.discountAmount : 0;
        price -= product.discountPercentage
            ? (product.price * product.discountPercentage) / 100
            : 0;

        return Number(price.toFixed(2));
    };

    const getTotalPriceOfSingleProduct = index => {
        //calculate package pricing
        let item = items[index];
        let cartQuantity = item.cartQuantity;
        let totalCost = 0;
        if (item.packagePrice) {
            // let result = []
            for (var i = 0; i < item.packagePrice.length && cartQuantity >= 1; i++) {
                if (cartQuantity >= item.packagePrice[i].piece) {
                    // result.push({...item.packagePrice[i], quantity: Math.floor(cartQuantity/item.packagePrice[i].piece)*item.packagePrice[i].piece})
                    totalCost +=
                        Math.floor(cartQuantity / item.packagePrice[i].piece) *
                        item.packagePrice[i].price;
                    cartQuantity %= item.packagePrice[i].piece;
                }
            }
        }
        return (
            Number((getProductPrice(index) * cartQuantity).toFixed(2)) + totalCost
        );
    };

    const getTotalDiscountAmountOfSingleProduct = index => {
        return (
            Number(items[index].price * items[index].cartQuantity) -
            getTotalPriceOfSingleProduct(index)
        ).toFixed(2);
    };
    console.log("updated items==", items)
    return (
        <Grid container spacing={10} justify="center">
            <Grid
                item
                xs={size.cart.xs}
                sm={size.cart.sm}
                md={size.cart.md}
                lg={size.cart.lg}
            >
                <Grid container>
                    {undoCount ? (
                        <Grid item xs={12}>
                            <hr />
                            <button onClick={undoDeletedItems} style={{ cursor: "pointer" }}>
                                Undo?
              </button>
                            <hr />
                        </Grid>
                    ) : null}

                    <Grid item>
                        {/* Container for title bar and order */}
                        <Grid container>
                            {/*<Grid item xs={12}>
                                <hr />
                                <Grid container>
                                    <Grid
                                        item
                                        xs={size.product.xs}
                                        sm={size.product.sm}
                                        md={size.product.md}
                                        lg={size.product.lg}
                                    >
                                        <div>Product</div>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={size.quantity.xs}
                                        sm={size.quantity.sm}
                                        md={size.quantity.md}
                                        lg={size.quantity.lg}
                                    >
                                        <div>Quantity</div>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={size.total.xs}
                                        sm={size.total.sm}
                                        md={size.total.md}
                                        lg={size.total.lg}
                                    >
                                        <div>Total</div>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={size.actions.xs}
                                        sm={size.actions.sm}
                                        md={size.actions.md}
                                        lg={size.actions.lg}
                                    >
                                        <div>Action</div>
                                    </Grid>
                                </Grid>
                                <hr />
                    </Grid>*/}
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell> PRODUCT</TableCell>
                                        <TableCell>QUANTITY</TableCell>
                                        <TableCell>TOTAL</TableCell>
                                        <TableCell>ACTION</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>


                                    {items.map((e, index) => (

                                        <Order
                                            key={e._id}
                                            product={e}
                                            deleteItem={() => setDeletedItemsHandler(e._id)}
                                            increaseQuantity={() => productIncreaseHandler(index)}
                                            decreaseQuantity={() => productDecreaseHandler(index)}
                                            productQuantity={(e, inValid) => setProductQuantityHandler(e, inValid, index)}
                                            productPrice={getProductPrice(index)}
                                            totalPrice={getTotalPriceOfSingleProduct(index)}
                                            totalDiscount={getTotalDiscountAmountOfSingleProduct(index)}
                                            gridSize={size}
                                        />

                                    ))}

                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

            <Grid
                item
                xs={size.cartSummery.xs}
                sm={size.cartSummery.sm}
                md={size.cartSummery.md}
                lg={size.cartSummery.lg}
            >
                <CartSummary
                    totalItem={items.length}
                    totalCostWithoutDiscount={totalCostWithoutDiscountGenerator()}
                    totalCostWithDiscount={totalCostWithDiscountGenerator()}
                    totalDiscount={totalDiscountGenerator()}
                    shippingCost={props.shippngCost ? props.shippngCost : 0}
                    products={products}
                />
            </Grid>
        </Grid>
    );
}

const sizeObject = {
    product: {
        xs: 4,
        sm: 4,
        md: 5,
        lg: 6,
        img: {
            xs: 12,
            sm: 12,
            md: 4,
            lg: 4
        },
        name: {
            xs: 12,
            sm: 12,
            md: 6,
            lg: 8
        }
    },
    quantity: {
        xs: 3,
        sm: 3,
        md: 5,
        lg: 2
    },
    total: {
        xs: 3,
        sm: 3,
        md: 2,
        lg: 2
    },
    actions: {
        xs: 2,
        sm: 2,
        md: 2,
        lg: 2
    },

    // ......... //
    cart: {
        xs: 12,
        sm: 7,
        md: 7,
        lg: 7
    },
    cartSummery: {
        xs: 12,
        sm: 4,
        md: 4,
        lg: 3
    }
};
