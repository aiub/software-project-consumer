import Home from "../views/home/home";
import Cart from "../views/cartView/cart/cart";
import * as path from "./slugs";

const routes = [
  {
    path: path.HOME_PATH,
    name: "HOME",
    component: Home
  },
  {
    path: path.PACHPAI_SHOP_PATH,
    name: "PACHPAI SHOP",
    component: null,
  },
  {
    path: "/blog",
    name: "BLOG",
    component: null,
  },
  {
    path: path.SELL_PRODUCT_PATH,
    name: "SELL PRODUCT",
    component: null,
  },
  {
    path: path.CUSTOMER_SERVICE_PATH,
    name: "CUSTOMER SERVICE",
    component: null,
  },
  {
    path: path.CART_INFO_PATH,
    name: "CART",
    component: Cart,
  }
];

export default routes;
