export const ROOT_PATH = '/'
export const MARKET_PLACE_PATH = `${ROOT_PATH}market-place/`;

export const HOME_PATH = `${MARKET_PLACE_PATH}home`;
export const PRODUCT_INFO_PATH = `${ROOT_PATH}product-info`;
export const CART_INFO_PATH = `${MARKET_PLACE_PATH}cart`;
export const PACHPAI_SHOP_PATH = `${MARKET_PLACE_PATH}pachpai-shop`;
export const CUSTOMER_SERVICE_PATH = `${MARKET_PLACE_PATH}customer-service`;
export const SELL_PRODUCT_PATH = `${MARKET_PLACE_PATH}sell-product`;

export const LOGIN_PATH = `${ROOT_PATH}login`;
export const SIGNUP_PATH = `${ROOT_PATH}signup`;

