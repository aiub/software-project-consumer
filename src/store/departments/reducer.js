import * as actionTypes from "./actionTypes";

const initialState = {
    departments: [],
    productLoadingError: "",
    loading: false

}

export default function (state = initialState, action) {
    switch (action.type) {
        case actionTypes.GET_HOT_DEPARTMENTS:
            state = {
                ...state,
                departments: action.departments,
                loading: false
            }
            break;

        default:
            return state;

    }
    return state;
}