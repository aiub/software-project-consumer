import axios from "axios";
import * as actionTypes from "./actionTypes";
import * as URL from "../../helpers/apis";

export const getCartProductsAction = async id => {
    let cartItems = await axios.get(URL.GET_CART_URL).then(res => res.data).catch(err => console.log("error in getCartProductsAction in stores", err.message));

    return { type: actionTypes.GET_CART, cartItems: cartItems }
}

export const postCartProductsAction = async (cartItem, cart) => {
    await axios.post(URL.POST_PRODUCT_TO_CART_URL, cartItem).then(res => res.data).catch(err => console.log("error in post product to cart in stores", err.message));

    return { type: actionTypes.POST_PRODUCT_TO_CART, cartItem: cartItem, variant: cart.variant, product: cartItem }
}

export const postOrderAction = async (order) => {
    await axios.post(URL.POST_ORDER_URL, order).then(res => res.data).catch(err => console.log("error in post product to Database in stores", err.message));

    return { type: actionTypes.POST_ORDER }
}