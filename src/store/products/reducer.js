import * as actionTypes from "./actionTypes";

const initialState = {
  productAdditionalInfo: "",
  productDescription: "",
  productReviews: "",
  shippingInfo: "",

}
export default function (state = initialState, action) {

  switch (action.type) {


    case actionTypes.GET_PRODUCT_ADDITIONAL_INFO:

      state = {
        ...state,
        productAdditionalInfo: action.productAdditionalInfo
      }
      break;

    case actionTypes.GET_PRODUCT_DESCRIPTION:

      state = {
        ...state,
        productDescription: action.productDescription
      }
      break;

    case actionTypes.GET_PRODUCT_REVIEWS:

      state = {
        ...state,
        productReviews: action.productReviews
      }
      break;

    case actionTypes.GET_SHIPPING_INFO:

      state = {
        ...state,
        shippingInfo: action.shippingInfo
      }
      break;

    default:
      return state;
  }


  return state;


}

